#!/usr/bin/lua5.3

local cjson     = require("cjson")
local publish   = require("mqtt/publish")

local hostname  = "msg.alpinelinux.org"
local mqtt_user = os.getenv("WEBHOOK_MQTT_USER")
local mqtt_pass = os.getenv("WEBHOOK_MQTT_PASSWORD")

local json      = arg[1]
local header    = arg[2]
local data      = cjson.decode(json)

-- send the payload to mqtt
local function publish_payload(topic, payload, retain)
	publish.tls_set("/etc/ssl/cert.pem")
	publish.login_set(mqtt_user, mqtt_pass)
	publish.single(topic, payload, nil, retain, hostname, "8883")
end

-- we are only interested in system hooks
if (header ~= "System Hook") then
	io.write("Not implemented")
	os.exit(0)
end

-- default events always send by gitlab
local events = {
	{name = "project_create", arg = "path_with_namespace"},
	{name = "project_destroy", arg = "path_with_namespace"},
	{name = "project_rename", arg = "path_with_namespace"},
	{name = "project_transfer", arg = "path_with_namespace"},
	{name = "project_update", arg = "path_with_namespace"},
	{name = "user_add_to_team", arg = "project_path_with_namespace"},
	{name = "user_remove_from_team", arg = "project_path_with_namespace"},
	{name = "user_update_for_team", arg = "project_path_with_namespace"},
	{name = "user_create", arg = "username"},
	{name = "user_destroy", arg = "username"},
	{name = "user_failed_login", arg = "username"},
	{name = "user_rename", arg = "username"},
	{name = "key_create", arg = "username"},
	{name = "key_destroy", arg = "username"},
	{name = "group_create", arg = "path"},
	{name = "group_destroy", arg = "path"},
	{name = "group_rename", arg = "path"},
	{name = "user_add_to_group", arg = "group_path"},
	{name = "user_remove_from_group", arg = "group_path"},
	{name = "user_update_for_group", arg = "group_path"}
}

for _,event in ipairs(events) do
	if event.name == data.event_name then
		publish_payload(("gitlab/%s/%s"):format(event.name, data[event.arg]),json)
		io.write(("%s message has been send"):format(event.name))
	end
end

-- specific events which need to be anabled
if (data.event_name == "push" or data.event_name == "tag_push") then
	publish_payload(("gitlab/%s/%s"):format(data.event_name,
		data.project.path_with_namespace), json)
	io.write(("%s message has been send"):format(data.event_name))
	-- retrained msg for builders backwards compatability
	if data.project.path_with_namespace == "alpine/aports" then
		local branch = data.ref:match("^refs/heads/(.*)")
		local project = data.project.path_with_namespace
		local size = #data.commits or 0
		publish_payload(("git/aports/%s"):format(branch), cjson.encode({
			ref = data.ref, repo = project, head = data.after, gitlab = true,
			size = size, user = data.user_username}), true)
		io.write(("%s message has been send (compat)"):format(data.event_name))
	end
elseif data.event_name == "merge_request" or
	data.event_name == "repository_update" then
		publish_payload(("gitlab/%s/%s"):format(data.event_name,
			data.project.path_with_namespace), json)
		io.write(("%s message has been send"):format(data.event_name))
end
